import { Button } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

// export default function CourseCard(props) {
export default function CourseCard({ courseProp, courseCountProp }) {
  // console.log("Contents of props");
  // console.log(coursesData);

  const { name, description, price, _id } = courseProp;
  // const [count, setCount] = useState(0);
  // const [slot, setSlot] = useState(30);

  // function enroll() {
  //   if (slot > 0) {
  //     setSlot(slot - 1);
  //     setCount(count + 1);
  //   } else {
  //     alert("No more seats");
  //   }
  //   console.log("Slots " + slot);
  //   console.log("Enrolees " + count);
  // }

  //useEffect always runs the task on the initial rendering
  // useEffect(() => {
  //   if (slot === 0) {
  //     alert("No more seats!");
  //   }
  // }, [slot]);

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle className="">Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price: </Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Button variant="primary" as={Link} to={`/courses/${_id}`}>
          Enroll
        </Button>
        {/* <Card.Text>Total Enrolled: {count}</Card.Text> */}
      </Card.Body>
    </Card>
  );
}

CourseCard.propTypes = {
  courseProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
