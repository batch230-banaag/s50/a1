import Banner from "../components/Banner";

export default function ErrorPage() {
  const data = {
    title: "Page Not Found",
    content: "We're sorry, but the page you're looking for could not be found.",
    destination: "/",
    label: "Back home",
  };
  return <Banner data={data} />;
}
