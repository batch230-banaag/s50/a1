import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
// import FormControl from "@mui/material/FormControl";
// import InputLabel from "@mui/material/InputLabel";
// import InputAdornment from "@mui/material/InputAdornment";
// import IconButton from "@mui/material/IconButton";
// import OutlinedInput from "@mui/material/OutlinedInput";
import {
  FormControl,
  InputLabel,
  InputAdornment,
  IconButton,
  OutlinedInput,
} from "@mui/material";
import Swal from "sweetalert2";

export default function Login() {
  // 3 - use the state from App.js
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setpassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  // const showPasswordChange = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  console.log(password);

  // function loginUser(e) {
  //   e.preventDefault();

  //   localStorage.getItem("email", email);

  //   setUser({
  //     email: localStorage.setItem("email", email),
  //   });

  //   setEmail("");
  //   setpassword("");

  //   console.log(`${email} has been verified! Welcome back!`);
  //   alert("Successfully logged in");
  // }

  function loginUser(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        console.log("Check access token");
        console.log(data.accessToken);

        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          localStorage.setItem("email", email);
          retrieveUserDetails(data.accessToken);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt!",
          });
        } else {
          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login details and try again.",
          });
        }
      });
    setEmail("");
  }

  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          email: data.email,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(event) => loginUser(event)}>
      <h2>Login</h2>
      {/* <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </Form.Group> */}

      {/* <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type={showPassword ? "text" : "password"}
          placeholder="Password"
          value={password}
          onChange={(e) => setpassword(e.target.value)}
          required
        />

        <Form.Check
          type="switch"
          label="show password"
          checked={showPassword}
          onChange={showPasswordChange}
          className="mt-2"
        />
      </Form.Group> */}
      <FormControl fullWidth sx={{ mt: 3 }} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-password">Email</InputLabel>
        <OutlinedInput
          label="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type={"text"}
        />
      </FormControl>

      <FormControl fullWidth sx={{ mt: 2, mb: 1 }} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
        <OutlinedInput
          value={password}
          onChange={(e) => setpassword(e.target.value)}
          id="outlined-adornment-password"
          type={showPassword ? "text" : "password"}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
          label="Password"
        />
      </FormControl>

      {isActive ? (
        <Button variant="success" type="submit" id="submitBtn" className="mt-3">
          Login
        </Button>
      ) : (
        <Button
          variant="success"
          type="submit"
          id="submitBtn"
          className="mt-3"
          disabled
        >
          Login
        </Button>
      )}
    </Form>
  );
}
