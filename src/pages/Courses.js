import { Fragment, useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import CourseCard from "../components/CourseCard";

import { Navigate } from "react-router-dom";

export default function Courses() {
  console.log("Contents of coursesData");
  // console.log(coursesData[0]);

  const { user } = useContext(UserContext);
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCourses(
          data.map((course) => {
            return <CourseCard key={course.id} courseProp={course} />;
          })
        );
      });
  },[]);

  // const courses = coursesData.map((course) => {
  //   return (
  //     <CourseCard
  //       key={course.id}
  //       courseProp={course}
  //       courseCountProp={courseCount}
  //     />
  //   );
  // });
  return user.isAdmin ? (
    <Navigate to="/admin" />
  ) : (
    <>
      <h1>Courses</h1>
      {courses}
    </>
  );
}
