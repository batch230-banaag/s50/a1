import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  console.log(password1);

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    }).then((res) => {
      if (res) {
        setFirstName("");
        setLastName("");
        setEmail("");
        setMobileNumber("");
        setPassword1("");
        setPassword2("");

        Swal.fire({
          title: "Email already exists",
          icon: "error",
          text: "Please try another email.",
        });
      } else {
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password1,
            mobileNumber: mobileNumber,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            if (data) {
              Swal.fire({
                title: "Successfully registered",
                icon: "success",
                text: "You have successfully registered.",
              });
              navigate("/login");
            } else {
              Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again.",
              });
            }
          });
      }
    });

    
  }

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNumber.length >= 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNumber, password1, password2]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(event) => registerUser(event)}>
      <Form.Group controlId="useFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="useLastName">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="userNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter mobile number"
          value={mobileNumber}
          onChange={(e) => setMobileNumber(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>
      {isActive ? (
        <Button
          variant="primary"
          type="submit"
          id="submitBtn"
          className="mt-3"
          onClick={registerUser}
        >
          Register
        </Button>
      ) : (
        <Button
          variant="primary"
          type="submit"
          id="submitBtn"
          className="mt-3"
          disabled
        >
          Register
        </Button>
      )}
    </Form>
  );
}
